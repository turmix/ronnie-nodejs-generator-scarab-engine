/** UTF-8 - ő */
'use strict';

var util 	= require('util')
,	path 	= require('path')
,	yeoman 	= require('yeoman-generator')
,	lib		= require('../lib/lib')
;

var ModelGenerator = function ModelGenerator(args, options, config) {
	yeoman.generators.NamedBase.apply(this, arguments);
	this.baseDir 	= lib.baseDir(process.cwd(), 'generator.json');
	this.name 		= arguments[0].join(' ');
	this.date 		= new Date();
	this.pkg 		= JSON.parse(this.readFileAsString(path.join(this.baseDir, 'generator.json')));
};

util.inherits(ModelGenerator, yeoman.generators.NamedBase);

ModelGenerator.prototype.askFor = function askFor() {
	var cb = this.async();

	var prompts = [
		{
			'type'		: 'list'
		,	'name'		: 'modelType'
		,	'message'	: 'Type of model:'
		,	'choices'	: [
				'MySQL'
			,	'MongoDB'
			]
		}
	];

	this.prompt(prompts, function (props) {
		this.modelType = (props.modelType == 'MongoDB' ? 'mongo' : 'mysql');

		this.name = (function(model){
			model = lib.camelcase(model, true);
			model = model.replace(/^\s+|\s+$/g, "");
			return model;
		})(this.name);

		cb();
	}.bind(this));
	/*
	var cb = this.async();

	var prompts = [
		{
			'type'		: 'list'
		,	'name'		: 'modelSide'
		,	'message'	: 'Model for:'
		,	'choices'	: [
				'Server'
			,	'Client'
			]
		}
	];

	this.prompt(prompts, function (props) {
		this.modelSide = props.modelSide;
		cb();
	}.bind(this));*/
};

ModelGenerator.prototype.files = function files() {
	// DB model
	this.template(
		this.modelType + '.ejs'
	,	this.baseDir + this.pkg.path.model[this.modelType] + '/model' + this.name + '.js'
	);
};

module.exports = ModelGenerator;