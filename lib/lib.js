var path 	= require('path')
,	fs		= require('fs')
;

exports.capitalize = function(str, allWords){
	if (allWords) {
		return str.split(' ').map(function(word){
			return exports.capitalize(word);
		}).join(' ');
	}
	return str.charAt(0).toUpperCase() + str.substr(1);
};

exports.camelcase = function (str, uppercaseFirst){
	return str.replace(/[^\w\d ]+/g, '').split(' ').map(function(word, i){
		if (i || (0 == i && uppercaseFirst)) {
			word = exports.capitalize(word);
		}
		return word;
	}).join('');
};

exports.baseDir = function(dir, fileName, deep) {
	fileName = './' + fileName;

	deep || (deep = 16);
	--deep;

	if(deep <= 0){
		return '';
	}

	if ( !fs.existsSync( path.resolve(dir, fileName) ) ) {
		return exports.baseDir( path.normalize(dir + '/..'), fileName, deep );
	} else {
		return dir + '/';
	}
};